FROM gitpod/workspace-full

# Upgrade add dependencies
RUN sudo apt-get update && \
  sudo apt-get dist-upgrade -y && \
  sudo apt-get install -y libvips-dev libgl-dev && \
  sudo rm -rf /var/lib/apt/lists/*


# Setup node version 12
ENV NODE_VERSION=12.22.6
RUN sudo apt-get install -y curl
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.38.0/install.sh | bash
ENV NVM_DIR=${HOME}/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/${HOME}/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"

# cache sharp c-modules for faster builds
RUN  npm install --global --build-from-source --unsafe-perm sharp

RUN node --version
RUN npm --version

