# build requires <=12.x
FROM node:12 as build

# Add build dependencies
RUN apt-get update && \
  apt-get dist-upgrade -y && \
  apt-get install -y libvips-dev libgl-dev && \
  rm -rf /var/lib/apt/lists/*

# Copy workspace file
# be sure to add a .dockerignore that excludes local artifacts
WORKDIR /workspace
ADD . .

RUN npm install --build-from-source --unsafe-perm
RUN NODE_ENV=production npm run build

FROM nginx:alpine

COPY --from=build /workspace/public /usr/share/nginx/html

